# pic32mzserverdemo

This is a demonstration project for the **Belcarra PIC32MZ USBLAN Driver**. It demonstrates how **Networking over USB** can be used instead of **Ethernet** or
**WiFi** to provide networking to a Microchip PIC32MZ based board.

This project is based on the **Microchip Harmony 2 Framework demonstration project**:

    - **apps/tcpip/tcpip_tcp_client_server**

The project has been modified to add the following Harmony 2 tcpip components:

    * Demonstration http server
    * Telnet
    * Iperf

The project configuration has been changed:

    * The System console changed from USB CDC ACM to a USART console at 460800 baud

Added:

    * The **Belcarra PIC32MZ USBLAN Driver** can be used if installed in the Harmony 2 Framework drivers directory.

## Supported Microchip Evaluation Boards

    - **PIC32MZ EF Ethernet Starter Kit** (pic32mz_ef_sk)
    - **PIC32MZ EF Curiosity Board Bundle **(pic32mz_ef_curiosity)
    - **PIC32MZ EC Ethernet Starter Kit** (pic32mz_ec_sk).

## Networking Over USB - CDC-EEM

**Networking over USB** uses a USB connection to transfer TCP IP Frames (aka packets) between the USB Device (in this case a Microchip Evaluation Board) and a
USB Host (typically a Windows, Mac or Linux system.)

There are various protocols defined by USB.org:

    * CDC-ECM
    * CDC-EEM
    * CDC-NCM

The **Belcarra PIC32MZ USBLAN Driver** implements **CDC-EEM**. The **CDC-EEM** protocol allows for simple point to point transfer of frames between the USB
Host and the USB Device. It supports frame aggregation and his very little overhead.

The **USBLAN** driver supports zero-copy received frames. I.e. the data is received into a receive buffer by the USB High Speed driver and that buffer is given
to the TCPIP layer for processing.

The **USBLAN** driver also supports zero-copy transmit frames or can aggregate multiple frames depending on how the data is formatted by the TCPIP lyaer.




## Performance

Using iperf the pic32mz_ef_sk board can source TCP data at about **70-80 Mbits/second with Ethernet** and about **60-70 Mbits/second using USBLAN**.

## Belcarra PIC32MZ USBLAN Driver

The _usblan_ configurations require the **Belcarra PIC32MZ USBLAN Driver**. That driver is available under license from Belcarra Technologies.

Contact Email: [info@belcarra.com](mailto://info@belcarra.com)


## Microchip Eval Boards 

### PIC32MZ_EF_SK - Ethernet Starter Kit PIC32MZ2048EFM144 - [DM320007-C](https://www.digikey.ca/product-detail/en/microchip-technology/DM320007-C/DM320007-C-ND/5401235)

Configurations:

    - pic32mz_ef_sk_usblan - USBLAN and Ethernet
    - pic32mz_ef_sk - Ethernet only 

![pic32mz_ef_sk][pic32mz_ef_sk]


### PIC32MZ_EF_CURIOSITY - Curiosity Board PIC32MZ2048EFM100 - [DM320104-BNDL](https://www.digikey.ca/product-detail/en/microchip-technology/DM320104-BNDL/DM320104-BNDL-ND/9094799a)

**N.B. the USB UART is shown plugged into Mikro Bus #1. The project configuration requires it to be in Mikro Bus #2.**

Configurations:

    - pic32mz_ef_sk_curiosity_usblan - USBLAN and Ethernet
    - pic32mz_ef_sk_curiosity - Ethernet only

![pic32mz_ef_curiosity][pic32mz_ef_curiosity] 


### PIC32MZ_EC_SK - Ethernet Starter Kit PIC32MZ2048ECH144 - DM320006

Configurations:

    - pic32mz_ec_sk_usblan - USBLAN and Ethernet
    - pic32mz_ec_sk - Ethernet only

![pic32mz_ec_sk][pic32mz_ec_sk] 


N.B. The older pic32mz_ec_sk board does not have a USB2UART port. Telnet to the boards IP address through Ethernet or USBLAN will get the System Command
console (login: admin, password: microchip).

### PIC32MX_EF_SK2 - Ethernet Starter Kit II PIC32MX795F512L - [DM320004-2](https://www.digikey.ca/product-detail/en/microchip-technology/DM320004-2/DM320004-2-ND/4759053i)

The older PIC32MX Ethernet Starter Kit 2 (pic32mx_eth_sk2) do not work. 
Enabling the USB driver appears to allocate too much memory for the TCPIP stack to load (fails trying to allocate memory on the heap.)

![pic32mx_eth_sk2][pic32mx_eth_sk2]



[pic32mz_ef_sk]: /img/pic32mz_ef_sk.png
[pic32mz_ef_curiosity]: /img/pic32mz_ef_curiosity.png
[pic32mx_eth_sk2]: /img/pic32mx_eth_sk2.png
[pic32mz_ec_sk]: /img/pic32mz_ec_sk.png

## Configurations

    - pic32mz_ef_sk_usblan - USBLAN and Ethernet
    - pic32mz_ef_sk_curiosity_usblan - USBLAN and Ethernet
    - pic32mz_ec_sk_usblan - USBLAN and Ethernet
    - pic32mz_ef_sk - Ethernet only
    - pic32mz_ef_sk_curiosity - Ethernet only
    - pic32mz_ec_sk - Ethernet only


## Testing Setup

Use two systems:
    1. Linux development and testing for Ethernet
    2. Windows for testing USBLAN

Linux setup:
    - Chrome with two tabs open, for the two possible IP addresses that may be used for the Ethernet (depending if the Ethernet is the first or second interface). E.g.: http://192.168.40.145/ - Ethernet only configurations http://192.168.40.146/ - USBLAN and Ethernet configurations
    - Terminal window open running minicom pointing at appropriate serial port, configured to 460800 baud. Port names may vary, but on my system: /dev/ttyACM0 - PIC32MZ EF SK USB2UART port /dev/tty/USB0 - PIC32MZ EF Curiosity USBUART click board port

Windows setup:
    - Chrome with one tab open, set to the USBLAN IP address. E.g: http://192.168.188.2/
    - Terminal window (e.g. Cygwin mintty). Various tests, but typically: ping 192.168.188.2

The Chrome tabs should display the following screen, with the Random Number being updated rapidly:

![Web Server Screen](/img/demo-webserver.png "web server")

For the non _usblan_ configurations you will only see the webpage on the Ethernet IP address. 

IP address:

    - Ethernet is assigned by DHCP from your local DHCP server, see it's DHCP Lease table
    - USBLAN will be at 192.168.188.2


On the Starter Kits only buttons 1 and 2 work. Button 3 functionality is lost because the MPU pin it is on is used for the USB2UART Tx line.

On the Curiosity board, there is only one button. 



## MPLAB X Setup

This project has mixed file paths. 

    - All references to files outside of the project are *absolute*, e.g. /HarmonyFramework2/framework/usb/
    - All references to files within the project are *relative* to the project directory, e.g. ../src/app.configuration

Currently the project file assumes that the Harmony 2 Framework is installed in:

> /HarmonyFramework2






## Microchip Configurator

The Microchip Configurator is not currently working with this project. The configurator does not like the "empty" configurations. 

The alternet _usblan_ configurations are a "duplicate" of the base configuration. They differ only by defining *-DUSBLAN* in the gcc configuration. That
enables the use of the usblan driver.


## PIC32MZ EF Curiosity - Enabling Rx/Tx to Mikro Bus slots

The base configuration for the pic32mz_ef_sk_curiosity did not have USART #1 and #2 pins on the MPU enabled.

The following pins were added to the pin configuration:

| PIC32MZ EF Curiosity Tx/RX Pins | |
| Pin | Signal |
| 28 | RA9/RST |
| 48 | RPD15/U1TX |
| 69 | RPD10/U1RX |
| 39 | RF13/INT1/U1RTS |
|  2 | RA5/RST |
|  7 | RPC/U2TX |
|  8 | RPC3/U2RX |
| 40 | RF12/INT2 |


## MPFS2 Web Pages

To regenerate web pages:

> Java -jar /HarmonyFramework2/utilities/mpfs_generator/mpfs2.jar&

Then:
    - Browse to select source directory (e.g. firmware/src/web_pages/)
    - Select the radio button for PIC32 Image
    - The default Advanced Settings appear to work OK.
    - Browse to select the project directory (e.g. firmware/src)
    - Ensure the Image Name field contains required filename (e.g. mpfs_img2)
    - Click on Generate, verify size appears appropriate

That should generate the required file, e.g. mpfs_img2.c






## Release Notes

### 2019-10-02
    - Working configurations for pic32mz_ef_sk, pic32mz_ef_curiosity, pic32mz_ec_sk
    - Verified that pic32mx_eth_sk2 TCPIP failure when USB Function is enabled
    - Currently the Microchip Configurator is not working


### 2019-09-25 
      - Initial Checkin to verify that pic32mz_ef_sk project works
      - Remove self relative file links in configuration.xml



